from django.contrib import admin
from projects.models import Project

### imported the Project model from /projects/models.py/Project


@admin.register(Project)
class Project(admin.ModelAdmin):
    list_display = [
        "name",
        "description",
        "owner",
    ]
