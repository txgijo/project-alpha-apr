from django.db import models
from django.conf import settings

# Create your models here.


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(default=None)
    owner = models.ForeignKey(  ##### LOOKUP how this works
        settings.AUTH_USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,  ##### optional, False by default
    )

    def __str__(self):
        return self.name
