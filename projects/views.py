from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {  ### Project.objects.all()
        "list_projects": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    list = get_object_or_404(Project, id=id)
    context = {
        "list_tasks": list,
    }
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():  ### We should use the form to validate the values
            recipe = form.save(
                False
            )  ### and save them to the DB                        ##### added recipe = form.save(False)
            recipe.author = (
                request.user
            )  ### If good, redirect the browser                  ##### added to assig current user as author
            recipe.save()  ##### added to save recipe with author name
            return redirect(
                "list_projects"
            )  ### to another page and leave the function
    else:
        form = (
            ProjectForm()
        )  ### Create an instance of the Django model form class
        context = {  ### Put the form in the context
            "form": form,
        }  ### Render the HTML template with the form

    return render(request, "projects/create.html", context)


##### http://localhost:8000/projects/create/
