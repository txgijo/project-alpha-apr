from django.contrib import admin
from tasks.models import Task

### imported the Task model from /tasks/models.py/Task


@admin.register(Task)
class Task(admin.ModelAdmin):
    list_display = [
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    ]
