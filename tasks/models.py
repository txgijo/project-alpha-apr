from django.db import models
from django.conf import settings
from projects.models import (
    Project,
)  ### imported class Project from projects.models.py


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = (
        models.DateTimeField()
    )  ### DateTimeField not optional (null=True, blank=True makes optional)
    is_completed = models.BooleanField(
        default=False
    )  ### Boolean defaults to False
    project = models.ForeignKey(  ### ForeignKey
        Project,  ### Related to Project
        related_name="tasks",  ### Related name of "tasks"
        on_delete=models.CASCADE,  ### Optional, null=False by default
    )
    assignee = models.ForeignKey(  ### LOOKUP how this works
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
