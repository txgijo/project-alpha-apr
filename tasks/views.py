from django.shortcuts import render, redirect  # get_object_or_404
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTaskForm
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():  ### Form used to validate the user values
            form.save()  ### and save them to the DB
            return redirect(
                "list_projects"
            )  ### to another page and leave the function
    else:
        form = (
            CreateTaskForm()  ### Create an instance of the Django model form class
        )
        context = {  ### Put the form in the context
            "form": form,
        }
        ### Render the HTML template with the form
    return render(request, "tasks/create.html", context)


##### http://localhost:8000/tasks/create/


@login_required
def show_my_tasks(request):
    my_tasks_list = Task.objects.filter(assignee=request.user)
    context = {
        "my_tasks_list": my_tasks_list,
    }
    return render(request, "tasks/list.html", context)


#####  http://localhost:8000/tasks/mine/
